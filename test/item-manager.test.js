const ItemManager = artifacts.require("./ItemManager.sol");

contract("ItemManager", accounts => {

  it("should let you create new items", async () => {
    const itemMgr = await ItemManager.deployed();
    const itemName = "Item 1";
    const itemPrice = 500;

    const { logs } = await itemMgr.createItem(itemName, itemPrice, { from: accounts[0] });
    const [{ args }] = logs;
    const { itemIndex } = args;

    assert.equal(itemIndex, 0, "There should be one item index in there");

    const item = await itemMgr.items(itemIndex);

    assert.equal(item.identifier, itemName, "The item has a different identifier");
  });
});