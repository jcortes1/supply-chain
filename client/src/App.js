import { useState, useEffect } from "react";
import ItemManager from "./contracts/ItemManager.json";
// import Item from "./contracts/Item.json";
import getWeb3 from "./getWeb3";
import "./App.css";

function useFormInput(initialValue) {
  const [value, setValue] = useState(initialValue);

  function handleChange(e) {
    setValue(e.target.value);
  }

  return {
    value,
    onChange: handleChange
  }
}

function getWeb3Data(web3) {
  return Promise.all([
    web3,
    web3.eth.getAccounts(),
    web3.eth.net.getId()
  ]);
}

function handleState(setState) {
  return function state([web3, accounts, networkId]) {
    const itemMgrAddress = ItemManager.networks[networkId]?.address;
    // const itemAddress = Item.networks[networkId]?.address;
    const itemMgrContract = new web3.eth.Contract(ItemManager.abi, itemMgrAddress);
    // const itemContract = new web3.eth.Contract(Item.abi, itemAddress);
    const contracts = { itemMgrContract };
    setState({ loaded: true, web3, contracts, accounts });
    return itemMgrContract;
  };
}

function listenToPaymentEvent(itemMgrContract) {
  itemMgrContract?.events?.SupplyChainStep()
    .on("data", async function (e) {
      const step = parseInt(e?.returnValues?.step, 10);
      console.log("[STEP]", step);

      // Created: 0, Paid: 1, Delivered: 2
      if (step === 1) {
        const idx = e?.returnValues?.itemIndex;
        const item = await itemMgrContract.methods.items(idx).call();
        console.log(`Item ${item?.identifier} was paid, deliver it now!`);
      }
    });
}

function handleError(label = "generic") {
  return function log(error) {
    console.error(`[${label}!!!]`, error);
  };
}

function App() {
  const initialState = {
    loaded: false,
    web3: undefined,
    contracts: { itemMgr: undefined, item: undefined },
    accounts: []
  };
  const [state, setState] = useState(initialState);
  const { accounts, contracts, loaded } = state;

  const cost = useFormInput(0);
  const itemName = useFormInput("Item 1");

  useEffect(() => {
    getWeb3()
      .catch(handleError("getWeb3"))
      .then(getWeb3Data)
      .catch(handleError("getWeb3Data"))
      .then(handleState(setState))
      .catch(handleError("handleState"))
      .then(listenToPaymentEvent)
      .catch(handleError("listenToPaymentEvent"));
  }, []);

  async function handleSubmit() {
    const { itemMgrContract } = contracts;
    const [account] = accounts;

    try {
      const result = await itemMgrContract.methods
        .createItem(itemName.value, cost.value)
        .send({ from: account });
        // .send({ from: account, gas: 3000000 });
      console.log(`Send ${cost.value} Wei to ${result?.events?.SupplyChainStep?.returnValues?.addr}`);
      
    } catch (error) {
      console.error("[handleSubmit!!!]", error);
    }
  }

  if (!loaded) {
    return (
      <div>Loading Web3, Accounts and Contracts ...</div>
    );
  }
  
  return (
    <div className="App">
      <h1>Simply Payment/Supply Chain Example!</h1>
      <h2>Items</h2>
      <h2>Add Element</h2>
      Cost: <input {...cost} />
      Item Name: <input {...itemName} />
      <button type="button" onClick={handleSubmit}>Create Item</button>
    </div>
  );
}

export default App;
