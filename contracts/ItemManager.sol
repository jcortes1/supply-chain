// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.4;

// import "@openzeppelin/contracts/access/Ownable.sol";
import "../node_modules/@openzeppelin/contracts/access/Ownable.sol";
import "./Item.sol";

contract ItemManager is Ownable {
    enum SupplyChainSteps {Created, Paid, Delivered}
    struct SItem {
        Item item;
        SupplyChainSteps step;
        string identifier;
    }
    mapping(uint256 => SItem) public items;
    uint256 index;

    event SupplyChainStep(uint256 itemIndex, uint256 step, address addr);

    function createItem(string memory identifier, uint256 priceInWei)
        public
        onlyOwner
    {
        Item item = new Item(this, priceInWei, index);

        items[index].item = item;
        items[index].step = SupplyChainSteps.Created;
        items[index].identifier = identifier;

        emit SupplyChainStep(
            index,
            uint256(SupplyChainSteps.Created),
            address(item)
        );

        index += 1;
    }

    function triggerPayment(uint256 idx) public payable {
        Item item = items[idx].item;

        require(
            address(item) == msg.sender,
            "Only items are allowed to update themselves"
        );
        require(item.priceInWei() == msg.value, "Not fully paid yet");
        require(
            items[idx].step == SupplyChainSteps.Created,
            "Item is further in the supply chain"
        );

        items[idx].step = SupplyChainSteps.Paid;

        emit SupplyChainStep(
            idx,
            uint256(SupplyChainSteps.Paid),
            address(item)
        );
    }

    function triggerDelivery(uint256 idx) public onlyOwner {
        require(
            items[idx].step == SupplyChainSteps.Created,
            "Item is further in the supply chain"
        );

        items[idx].step = SupplyChainSteps.Delivered;

        emit SupplyChainStep(
            idx,
            uint256(SupplyChainSteps.Delivered),
            address(items[idx].item)
        );
    }
}
