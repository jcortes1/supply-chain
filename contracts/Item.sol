// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.4;

import "./ItemManager.sol";

contract Item {
    uint256 public priceInWei;
    uint256 public paidWei;
    uint256 public index;

    ItemManager itemMngrContract;

    constructor(
        ItemManager itemMngr,
        uint256 _priceInWei,
        uint256 idx
    ) {
        priceInWei = _priceInWei;
        index = idx;
        itemMngrContract = itemMngr;
    }

    receive() external payable {
        require(msg.value == priceInWei, "We don't support partial payments");
        require(paidWei == 0, "Item is already paid!");

        paidWei += msg.value;

        (bool success, ) =
            address(itemMngrContract).call{value: msg.value}(
                abi.encodeWithSignature("triggerPayment(uint256)", index)
            );

        require(success, "Delivery did not work");
    }

    fallback() external {}
}
